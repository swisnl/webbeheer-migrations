<?php
/**
 * Created by PhpStorm.
 * User: bjorn
 * Date: 14-4-2017
 * Time: 09:39.
 */

namespace WebbeheerMigrations;

class Migration
{
    /** @var string * */
    protected $type;

    /** @var string * */
    protected $class;

    /** @var string * */
    protected $filename;

    /** @var string * */
    protected $content;

    public function __construct(string $type, string $class, string $filename, string $content)
    {
        $this->type = $type;
        $this->class = $class;
        $this->filename = $filename;
        $this->content = $content;
    }

    public function type()
    {
        return $this->type;
    }

    public function class()
    {
        return $this->class;
    }

    public function filename()
    {
        return $this->filename;
    }

    public function content()
    {
        return $this->content;
    }
}
