<?php
/**
 * Created by PhpStorm.
 * User: bjorn
 * Date: 10-4-2017
 * Time: 14:37.
 */

namespace WebbeheerMigrations;

class Name
{
    protected $module;

    protected $columns = [];

    protected $indexName = '';

    protected $type;

    const CREATE_SCHEMA = 'create';
    const UPDATE_SCHEMA = 'update';
    const ADD_INDEX = 'index';
    const PROCEDURE = 'procedure';

    const MAX_COLUMNS_NAMING_LENGTH = 150;

    public function __construct(string $module, string $type)
    {
        $this->module = $module;
        $this->type = $type;
    }

    public function basename()
    {
        switch ($this->type) {
            case static::CREATE_SCHEMA:
                return $this->createSchema();
            case static::UPDATE_SCHEMA:
                return $this->updateSchema();
            case static::ADD_INDEX:
                return $this->createIndex();
            case static::PROCEDURE:
                return $this->createProdures();
            default:
        }
    }

    protected function createSchema()
    {
        return 'webbeheer_create_'.$this->module.'_table';
    }

    protected function updateSchema()
    {
        return 'webbeheer_update_'.$this->module.'_table_add'.$this->getColumnsPart().'_columns';
    }

    protected function createIndex()
    {
        return 'webbeheer_update_'.$this->module.'_table_index_'.$this->getIndexName();
    }

    protected function createProdures()
    {
        return 'webbeheer_create_'.$this->module.'_procedures';
    }

    public function classname()
    {
        $value = ucwords(str_replace(['-', '_'], ' ', $this->basename()));

        return str_replace(' ', '', $value);
    }

    public function filename()
    {
        return date('Y_m_d_His').'_'.$this->basename().'.php';
    }

    protected function getColumnsPart(): string
    {
        $columnPart = '';
        foreach ($this->getColumns() as $column) {
            if (mb_strlen($columnPart) + strlen($column) > static::MAX_COLUMNS_NAMING_LENGTH) {
                return $columnPart;
            }
            $columnPart .= '_'.$column;
        }

        return $columnPart;
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function setColumns(array $columns)
    {
        $this->columns = $columns;
    }

    public function getIndexName(): string
    {
        return $this->indexName;
    }

    public function setIndexName(string $indexName)
    {
        $this->indexName = $indexName;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
