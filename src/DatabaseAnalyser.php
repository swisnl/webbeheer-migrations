<?php

namespace WebbeheerMigrations;

use WebbeheerMigrations\Contracts\DatabaseAnalyserInterface;

class DatabaseAnalyser implements DatabaseAnalyserInterface
{
    /** @var \Webbeheer_Database */
    protected $database;
    protected $dbName;

    public function showColumns($module): array
    {
        $columns = $this->database->fetchAllColumn('SHOW COLUMNS FROM '.$this->escapeIdentifier($module), 'Field');

        return $columns === false ? [] : $columns;
    }

    protected function escapeIdentifier($table): string
    {
        return $this->database->quoteIdentifier($table);
    }

    protected function escape($table): string
    {
        return $this->database->quote($table);
    }

    public function showTables($name)
    {
        return $this->database->fetch('SHOW TABLES LIKE '.$this->escape($name));
    }

    public function countProcedures()
    {
        return $this->database->fetchAllColumn(
          'SHOW procedure STATUS WHERE Db = '.$this->escape($this->getDbName()).' AND Name IN (\'sm_insert_node\', \'sm_delete_node\', \'sm_move_node\')',
          'Name'
        );
    }

    public function currentIndexes($module)
    {
        $indexes = $this->database->fetchAllColumn(
            'SELECT    GROUP_CONCAT(COLUMN_NAME ORDER BY COLUMN_NAME) AS index_columns
                  FROM      information_schema.statistics 
                  WHERE     TABLE_SCHEMA = '.$this->escape($this->getDbName()).'
                  AND       TABLE_NAME = '.$this->escape($module).'
                  GROUP BY  INDEX_NAME',
            'index_columns'
        );

        array_walk($indexes, function (&$value, &$key) {
            if (strpos($value, ',') !== false) {
                $value = explode(',', $value);
            }
        });

        return $indexes;
    }

    public function getDatabase(): \Webbeheer_Database
    {
        return $this->database;
    }

    public function setDatabase(\Webbeheer_Database $database)
    {
        $this->database = $database;
        $this->setDbName($database->getConfig()['dbname']);
    }

    /**
     * @return mixed
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * @param mixed $dbName
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;
    }
}
