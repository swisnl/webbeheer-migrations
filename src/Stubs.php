<?php

namespace WebbeheerMigrations;

class Stubs
{
    public function column(string $stubfile, string $column): string
    {
        return $this->fill('columns/'.$stubfile, ['%column%' => $column]);
    }

    public function columnRaw(string $stubfile, string $column, string $table): string
    {
        return $this->fill('columns/'.$stubfile, ['%column%' => $column, '%table%' => $table]);
    }

    public function columnDrop(string $column): string
    {
        return $this->fill('schema/column-drop.stub', ['%column%' => $column]);
    }

    public function index(string $column, string $name): string
    {
        return $this->fill('schema/index-add.stub', ['%column%' => $column, '%index%' => $name]);
    }

    public function indexDrop(string $name): string
    {
        return $this->fill('schema/index-drop.stub', ['%index%' => $name]);
    }

    public function indexCompound(string $columns, string $name): string
    {
        return $this->fill('schema/index-add-compound.stub', ['%columns%' => $columns, '%index%' => $name]);
    }

    public function schema(string $table, array $columns, array $rawColumns = []): string
    {
        $stubfile = 'schema/create.stub';
        if (count($columns) === 0 && count($rawColumns) > 0) {
            $stubfile = 'schema/createRaw.stub';
        }

        return $this->fill($stubfile, [
            '%table%'      => $table,
            '%columns%'    => implode('', $columns),
            '%rawColumns%' => implode('', $rawColumns),
        ]);
    }

    public function schemaChange(string $table, array $columns, array $rawColumns = []): string
    {
        $stubfile = 'schema/change.stub';
        if (count($columns) === 0 && count($rawColumns) > 0) {
            $stubfile = 'schema/changeRaw.stub';
        }

        return $this->fill(
          $stubfile, [
            '%table%'      => $table,
            '%columns%'    => implode('', $columns),
            '%rawColumns%' => implode('', $rawColumns),
        ]);
    }

    public function migration(string $class, string $schemaUp, string $schemaDown)
    {
        return $this->fill('migration.stub', [
          '%class%'       => $class,
          '%schema_up%'   => $schemaUp,
          '%schema_down%' => $schemaDown,
        ]);
    }

    public function schemaDown(string $table): string
    {
        return $this->fill('schema/drop.stub', ['%table%' => $table]);
    }

    protected function load($stubfile = ''): string
    {
        $filepath = realpath(__DIR__.'/../resources/stubs/'.$stubfile);

        if (file_exists($filepath) === false) {
            $parts = explode('/', $stubfile);

            return $this->fill('columns/simple-column.stub', ['%type%' => str_replace('.stub', '', end($parts))]);
        }

        if (strpos(dirname($filepath), realpath(__DIR__.'/../resources/stubs/')) === 0) {
            return file_get_contents($filepath);
        }

        throw new \Exception('Invalid stub');
    }

    public function fill($stubfile, array $vars = []): string
    {
        $stub = $this->load($stubfile);

        return str_replace(
          array_keys($vars),
            $vars,
            $stub
        );
    }

    public function pivot($class, $table, $module)
    {
        $indexName = 'id_'.substr($module, 0, 58).'_id';

        return $this->fill('pivot.stub', [
            '%class%'     => $class,
            '%table%'     => $table,
            '%firstKey%'  => 'id',
            '%secondKey%' => $module.'_id',
            '%indexName%' => $indexName,
        ]);
    }
}
