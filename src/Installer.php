<?php

namespace WebbeheerMigrations;

use Webbeheer_Database;
use Webbeheer_Exception;
use Webbeheer_SqlReserved;
use WebbeheerMigrations\Contracts\InstallerInterface;

class Installer implements InstallerInterface
{
    protected static $disableInstaller = false;
    protected static $autoRunMigrations = true;
    protected static $autoAddMigrations = true;

    protected $migrationPath = 'database/migrations/';

    /** @var Stubs */
    protected $stubs;

    /** @var DatabaseAnalyser */
    protected $analyser;

    protected $columns;

    protected $indexes;

    protected $messages;

    protected $sqlMessages;

    protected $module;

    protected $tableCreated = false;

    protected $installedColumns = [];

    protected $migrations = [];

    const BIGINT = 'bigInteger.stub';
    const BINARY = 'binary.stub';
    const BOOLEAN = 'boolean.stub';
    const CHAR = 'char.stub';
    const DATE = 'date.stub';
    const DATETIME = 'dateTime.stub';
    const DECIMAL = 'decimal.stub';
    const DOUBLE = self::FLOAT;
    const ENUM = 'enum.stub';
    const FLOAT = 'float.stub';
    const INTEGER = 'integer.stub';
    const IPADDRES = 'ipAddress.stub';
    const JSON = 'json.stub';
    const JSONB = 'jsonb.stub';
    const LONGTEXT = 'longText.stub';
    const MACADDRESS = 'macAddress.stub';
    const MEDIUMINT = 'mediumInteger.stub';
    const MEDIUMTEXT = 'mediumText.stub';
    const SMALLINT = 'smallInteger.stub';
    const TEXT = 'text.stub';
    const TIME = 'time.stub';
    const TINYINT = 'tinyInteger.stub';
    const UNSIGNEDBIGINT = 'unsignedBigInteger.stub';
    const UNSIGNEDINT = 'unsignedInteger.stub';
    const UNSIGNEDMEDIUMINT = 'unsignedMediumInteger.stub';
    const UNSIGNEDSMALLINT = 'unsignedSmallInteger.stub';
    const UNSIGNEDTINYINT = 'unsignedTinyInteger.stub';
    const UUID = 'uuid.stub';

    const VARCHAR = 'string.stub';
    const TINYBLOB = 'tinyblob.stub';
    const GEOMETRY = 'geometry.stub';

    /**
     * Public constructor: init object.
     *
     * @param mixed                 $module
     * @param DatabaseAnalyser|null $analyser
     * @param Stubs|null            $stubs
     */
    public function __construct($module = MODULE, $analyser = null, $stubs = null)
    {
        if (class_exists('Swis_Functions') && \Swis_Functions::isLocalMachine() === false) {
            static::disableInstaller();
        }

        if (null === $analyser) {
            $this->analyser = new DatabaseAnalyser();
            $this->analyser->setDatabase(Webbeheer_Database::getInstance());
        } else {
            $this->analyser = $analyser;
        }

        $this->stubs = new Stubs();
        if (null !== $stubs) {
            $this->stubs = $stubs;
        }

        $this->columns = [];
        $this->indexes = [];
        $this->messages = [];
        $this->sqlMessages = [];
        $this->module = $module;
        $this->installedColumns = [];
    }

    /**
     * Init de kolom, bouw een array met kolommen op voor later gebruik.
     *
     * @param string $name       Naam van de kolom
     * @param string $columnType Kolomtype (Geldige MySQL kolom of validator van FormHandler)
     *
     * @throws Exception
     */
    public function initColumn($name, $columnType)
    {
        // error afhandeling
        if (strtolower($name) !== $name) {
            throw new Exception('Kolomnamen moeten lowercase zijn ('.$name.')');
        }
        if (in_array($name, ['id', 'gebruiker', 'groep'])) {
            throw new Exception($name.' is een gereserveerd woord binnen Webbeheer');
        }
        if (class_exists('Webbeheer_SqlReserved') && in_array(strtoupper($name), Webbeheer_SqlReserved::getReservedWords())) {
            throw new Exception($name.' is een gereserveerd SQL woord');
        }

        $this->columns[$name] = $this->getColumnType($columnType);
    }

    /**
     * Voeg index toe op kolom(men).
     *
     * @param string|array $column een kolom naam of een array met kolommen voor een gecombineerde index
     */
    public function addIndex($column)
    {
        if (is_array($column)) {
            sort($column);
        }
        if ($this->indexExists($column, $this->indexes) === false) {
            $this->indexes[] = $column;
        }
    }

    protected function indexExists($column, $indexes): bool
    {
        if (!is_array($column)) {
            return in_array($column, $indexes);
        }

        sort($column);
        foreach ($indexes as $index) {
            if (is_array($index) && $index === $column) {
                return true;
            }
        }

        return false;
    }

    public function addGebruikerGroep()
    {
        $tableColumns = $this->getTableColumns();
        if (!in_array('gebruiker', $tableColumns)) {
            $this->columns['gebruiker'] = $this->getColumnType('FH_INT');
        }
        if (!in_array('groep', $tableColumns)) {
            $this->columns['groep'] = $this->getColumnType('FH_INT');
        }
    }

    public function getTableColumns(): array
    {
        return $this->analyser->showColumns($this->module);
    }

    /**
     * Verwijder een kolom uit de array zodat deze niet aangemaakt wordt (wordt gebruikt bij elementen die geen eigen kolom hebben).
     *
     * @param string $name Naam van de kolom
     */
    public function unsetColumn($name)
    {
        unset($this->columns[$name]);
    }

    public function installRelationTable($tableName, $module)
    {
        if (self::$disableInstaller) {
            return;
        }

        $aTables = $this->analyser->showTables($tableName);
        // tabel bestaat niet
        if (!empty($aTables)) {
            return;
        }

        // error afhandeling
        if (strtolower($tableName) != $tableName) {
            throw new Webbeheer_Exception('Tabelnaam moet lowercase zijn ('.$tableName.')');
        }
        if (class_exists('Webbeheer_SqlReserved') && in_array(strtoupper($tableName), Webbeheer_SqlReserved::getReservedWords())) {
            throw new Webbeheer_Exception($tableName.' is een gereserveerd SQL woord');
        }

        $name = new Name($module.'_'.$tableName, Name::CREATE_SCHEMA);
        $class = $name->classname();
        $filename = $name->filename();
        $migration = $this->stubs->pivot($class, $tableName, $module);

        $this->migrations[] = new Migration(
          Name::CREATE_SCHEMA,
          $class,
          $filename,
          $migration
        );
    }

    public function install()
    {
        if (self::$disableInstaller) {
            return;
        }

        $tables = $this->analyser->showTables($this->module);

        if (!is_array($tables) || count($tables) === 0) {
            // tabel bestaat nog niet
            $this->installTable();
        } else {
            $this->updateTable();
        }

        $this->createIndexes();

        $this->writeMigrations();

        if (static::$autoRunMigrations === true && $this->pendingMigrations()) {
            $this->executeMigrations();
        }
    }

    protected function executeMigrations()
    {
        $kernel = \Webbeheer_Laravel::getKernel();
        $result = $kernel->call('migrate', ['--path' => $this->getMigrationPath(true), '-vvv' => '']);

        if ($result !== 0) {
            $this->messages[] = 'Er is wat misgegaan met het migreren';
            $this->sqlMessages[] = $kernel->output();

            return;
        }
        $this->messages[] = 'Migratie(s) uitgevoerd';
        $this->sqlMessages[] = $kernel->output();

        foreach ($this->getMigrations() as $migration) {
            $newName = dirname($this->getMigrationPath()).'/'.$migration->filename();

            rename($this->getMigrationPath().$migration->filename(), $newName);

            if (static::$autoAddMigrations === true) {
                exec(sprintf('git add %s', escapeshellarg($newName)));
            }
        }

        $this->tableCreated = true;
    }

    protected function writeMigrations()
    {
        if (!defined('ROOTPATH') || !is_dir(ROOTPATH.$this->migrationPath)) {
            throw new Exception('ROOTPATH is not defined or dir doesnt exist.');
        }

        if (!is_dir($this->getMigrationPath())) {
            mkdir($this->getMigrationPath());
        }

        foreach (new \DirectoryIterator($this->getMigrationPath()) as $fileInfo) {
            if (!$fileInfo->isDot()) {
                unlink($fileInfo->getPathname());
            }
        }

        foreach ($this->getMigrations() as $migration) {
            $success = file_put_contents($this->getMigrationPath().$migration->filename(), $migration->content());
            if ($success === false) {
                throw new Exception('Cannot write migration.');
            }
        }
    }

    protected function updateTable()
    {
        // kolommen toevoegen
        $tableColumns = $this->getTableColumns();

        // Bepaal welke kolommen niet bestaan in tabel
        $columns = array_diff(array_keys($this->columns), $tableColumns);
        if (count($columns) === 0) {
            return;
        }

        $columnsUp = [];
        $columnsDown = [];
        $columnsRaw = [];
        foreach ($this->columns as $name => $stub) {
            if (!in_array($name, $tableColumns)) {
                if ($stub === static::GEOMETRY) {
                    $columnsRaw[$name] = $this->stubs->columnRaw($stub, $name, $this->module);
                } else {
                    $columnsUp[$name] = $this->stubs->column($stub, $name);
                }
                $columnsDown[$name] = $this->stubs->columnDrop($name);
            }
        }

        $schemaUp = $this->stubs->schemaChange($this->module, $columnsUp, $columnsRaw);
        $schemaDown = $this->stubs->schemaChange($this->module, $columnsDown);

        $name = new Name($this->module, Name::UPDATE_SCHEMA);
        $name->setColumns(array_keys(array_merge($columnsUp, $columnsRaw)));
        $class = $name->classname();
        $filename = $name->filename();

        $migration = $this->stubs->migration($class, $schemaUp, $schemaDown);

        $this->migrations[] = new Migration(
          Name::UPDATE_SCHEMA,
          $class,
          $filename,
          $migration
        );
    }

    protected function installTable()
    {
        $columnsUp = [];
        $columnsRaw = [];

        $this->columns['gebruiker'] = static::INTEGER;
        $this->columns['groep'] = static::INTEGER;

        $defaults = [
          'gebruiker' => static::INTEGER,
          'groep'     => static::INTEGER,
        ];

        $this->columns = array_merge($defaults, $this->columns);

        foreach ($this->columns as $name => $stub) {
            if ($stub === static::GEOMETRY) {
                $columnsRaw[$name] = $this->stubs->columnRaw($stub, $name, $this->module);
            } else {
                $columnsUp[$name] = $this->stubs->column($stub, $name);
            }
        }
        $schemaUp = $this->stubs->schema($this->module, $columnsUp, $columnsRaw);
        $schemaDown = $this->stubs->schemaDown($this->module);

        $name = new Name($this->module, Name::CREATE_SCHEMA);
        $name->setColumns(array_keys(array_merge($columnsUp, $columnsRaw)));
        $class = $name->classname();
        $filename = $name->filename();

        $migration = $this->stubs->migration($class, $schemaUp, $schemaDown);

        $this->migrations[] = new Migration(
          $name->getType(),
          $class,
          $filename,
          $migration
        );
    }

    protected function createIndexes()
    {
        if (count($this->indexes) === 0) {
            return;
        }

        $currentIndexes = $this->analyser->currentIndexes($this->module);

        $indexesUp = [];
        $indexesDown = [];

        foreach ($this->indexes as $index) {
            if ($this->indexExists($index, $currentIndexes)) {
                continue;
            }

            $compound = false;
            $field = $index;
            $indexName = $field;

            if (is_array($index)) {
                $field = json_encode($index);
                $indexName = implode('__', $index);
                $compound = true;
            }

            if ($compound === true) {
                $indexesUp[$indexName] = $this->stubs->indexCompound($field, $indexName);
            } else {
                $indexesUp[$indexName] = $this->stubs->index($field, $indexName);
            }
            $indexesDown[] = $this->stubs->indexDrop($indexName);
        }

        if (count($indexesUp) === 0) {
            return;
        }

        $schemaUp = $this->stubs->schemaChange($this->module, $indexesUp);
        $schemaDown = $this->stubs->schemaChange($this->module, $indexesDown);

        $name = new Name($this->module, Name::ADD_INDEX);
        $name->setIndexName(implode('_', array_keys($indexesUp)));

        $class = $name->classname();
        $filename = $name->filename();

        $migration = $this->stubs->migration($class, $schemaUp, $schemaDown);

        $this->migrations[] = new Migration(
          $name->getType(),
          $class,
          $filename,
          $migration
        );
    }

    public function getInstalledColumns(): array
    {
        return $this->installedColumns;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function hasInstalledTable(): bool
    {
        return $this->tableCreated;
    }

    /**
     * Haal de uitgevoerde commandos op. Deze wordt gebruikt voor de meldingen boven de module.
     */
    public function getSql(): array
    {
        return $this->sqlMessages;
    }

    public function getProcedureStatus()
    {
        $aResult = $this->analyser->countProcedures();

        return count($aResult) === 3;
    }

    public function createProcedures()
    {
        $name = new Name('sm_nodes', Name::PROCEDURE);
        $class = $name->classname();
        $migration = $this->stubs->fill('structure-procedures.stub', ['%class%' => $class]);

        $this->migrations[] = new Migration(
          $name->getType(),
          $class,
          $name->filename(),
          $migration
        );

        $this->install();

        return $this->getProcedureStatus();
    }

    public function createLibraryModule($module, $titel, $rubriek = 3)
    {
        // TODO: Implement createLibraryModule() method?
    }

    public static function disableInstaller()
    {
        self::$disableInstaller = true;
    }

    public static function enableInstaller()
    {
        self::$disableInstaller = false;
    }

    public static function autoRunMigrations()
    {
        self::$autoRunMigrations = true;
    }

    public static function disableAutoRunMigrations()
    {
        self::$autoRunMigrations = false;
    }

    public static function autoAddMigrations()
    {
        self::$autoAddMigrations = true;
    }

    public static function disableAutoAddMigrations()
    {
        self::$autoAddMigrations = false;
    }

    /**
     * Haal een MySql kolomtype op aan de hand van een validator.
     *
     * @param string $type Het kolomtype (Geldige MySQL kolom of validator van FormHandler)
     */
    private function getColumnType($type): string
    {
        // Formhandler heeft een _ als prefix bij optionele validators
        $type = ltrim($type, '_');

        $validators = [
          'IsString'    => self::VARCHAR,
          'IsFilename'  => self::VARCHAR,
          'IsAlphaNum'  => self::VARCHAR,
          'IsAlpha'     => self::VARCHAR,
          'IsVariabele' => self::VARCHAR,
          'IsPassword'  => self::VARCHAR,
          'IsURL'       => self::VARCHAR,
          'IsEmail'     => self::VARCHAR,
          'IsEmailHost' => self::VARCHAR,
          'IsPostcode'  => self::VARCHAR,
          'IsPhone'     => self::VARCHAR,
          'notEmpty'    => self::VARCHAR,
          'IsDigit'     => self::TINYINT,
          'IsInteger'   => self::INTEGER,
          'IsFloat'     => self::FLOAT,
          'IsText'      => self::TEXT,
          'fh_date'     => self::DATE,
          'fh_datetime' => self::DATETIME,
          'fh_time'     => self::TIME,

          'FH_STRING'     => self::VARCHAR,
          'FH_FILENAME'   => self::VARCHAR,
          'FH_ALPHA_NUM'  => self::VARCHAR,
          'FH_ALPHA'      => self::VARCHAR,
          'FH_VARIABLE'   => self::VARCHAR,
          'FH_PASSWORD'   => self::VARCHAR,
          'FH_URL'        => self::VARCHAR,
          'FH_EMAIL'      => self::VARCHAR,
          'FH_EMAIL_HOST' => self::VARCHAR,
          'FH_POSTCODE'   => self::VARCHAR,
          'FH_PHONE'      => self::VARCHAR,
          'FH_NOT_EMPTY'  => self::VARCHAR,
          'FH_DIGIT'      => self::TINYINT,
          'FH_INTEGER'    => self::INTEGER,
          'FH_FLOAT'      => self::FLOAT,
          'FH_TEXT'       => self::TEXT,
          'FH_DATE'       => self::DATE,
          'FH_DATETIME'   => self::DATETIME,
          'FH_TIME'       => self::TIME,

          'GEOMETRY' => self::GEOMETRY,
          'TINYBLOB' => self::TINYBLOB,
          'BIGINT'   => self::BIGINT,
        ];

        if (array_key_exists($type, $validators)) {
            return $validators[$type];
        }

        $reflectionClass = new \ReflectionClass($this);
        $constants = $reflectionClass->getConstants();
        if (in_array($type, $constants)) {
            return $type;
        }

        // default varchar
        return self::VARCHAR;
    }

    /**
     * @return \WebbeheerMigrations\Migration[]
     */
    public function getMigrations(): array
    {
        return $this->migrations;
    }

    /**
     * @param \WebbeheerMigrations\Migration[] $migrations
     */
    public function setMigrations(array $migrations): array
    {
        $this->migrations = $migrations;
    }

    protected function getMigrationPath($relative = false): string
    {
        $path = $this->migrationPath.'webbeheer/';
        if ($relative === true) {
            return $path;
        }

        return ROOTPATH.$path;
    }

    protected function pendingMigrations(): bool
    {
        return count($this->migrations) > 0;
    }

    public function setMigrationPath(string $migrationPath)
    {
        $this->migrationPath = $migrationPath;
    }
}
