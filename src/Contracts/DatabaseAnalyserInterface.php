<?php
/**
 * Created by PhpStorm.
 * User: bjorn
 * Date: 13-4-2017
 * Time: 15:49.
 */

namespace WebbeheerMigrations\Contracts;

interface DatabaseAnalyserInterface
{
    public function showColumns($module): array;

    public function showTables($name);

    public function countProcedures();

    public function currentIndexes($module);
}
