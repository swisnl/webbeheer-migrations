<?php

namespace WebbeheerMigrations\Contracts;

interface InstallerInterface
{
    public function initColumn($name, $columnType);

    public function addIndex($column);

    public function addGebruikerGroep();

    public function unsetColumn($name);

    public function installRelationTable($tableName, $module);

    public function getTableColumns();

    public function install();

    public function getInstalledColumns();

    public function getMessages();

    public function hasInstalledTable();

    public function getSql();

    public function getProcedureStatus();

    public function createProcedures();

    public function createLibraryModule($module, $titel, $rubriek = 3);
}
